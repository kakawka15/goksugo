Rails.application.routes.draw do

  get 'home/mobile' => 'home#mobile'
  get 'tlen' => 'home#tlen'
  get 'new_year' => 'home#new_year'
  get 'new_year2' => 'home#new_year2'
  get 'api/scores' => 'api#scores'
  root 'home#index'

end
