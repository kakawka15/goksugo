# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'go_ksu_go'
set :user, 'kakawka15'
set :repo_url, 'git@bitbucket.org:kakawka15/goksugo.git'
set :deploy_to, "/home/hosting_kakawka/projects/goksugo/"

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads public/media}
# set :rvm_ruby_version, '2.1.5p273'

# set :linked_files, %w{config/database.yml}

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do

    end
  end

end
