class CreateStats < ActiveRecord::Migration
  def change
    create_table :stats do |t|
      t.integer :played, default: 0
      t.integer :high_score, default: 0
      t.integer :failure, default: 0

      t.timestamps null: false
    end
  end
end
