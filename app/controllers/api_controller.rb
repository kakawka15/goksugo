class ApiController < ApplicationController

  def scores
    @token = '148146220:AAG2wSYeRMUqOPoJMzj6iPScAb0Y4jV7RFE'
    require 'telegram/bot'

    Telegram::Bot::Client.run(@token) do |bot|
      bot.api.send_message(chat_id: 66990400, text: "ip: #{request.remote_ip}, score: #{params[:score]}")
    end
    Stat.first.update_attribute(:high_score, params[:score])
    render nothing: true
  end
end
