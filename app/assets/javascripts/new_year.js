//= require jquery
//= require jquery_ujs
//= require quintus.js
//= require quintus_2d.js
//= require quintus_anim.js
//= require quintus_audio.js
//= require quintus_input.js
//= require quintus_scenes.js
//= require quintus_sprites.js
//= require quintus_tmx.js
//= require quintus_touch.js
//= require quintus_ui.js

window.addEventListener("load",function() {

var Q = window.Q = Quintus({ development: true })
        .include("Sprites, Scenes, Input, 2D, Anim, Touch, UI, Audio")
        .setup({ maximize: true })
        .controls().touch().enableSound();

var SPRITE_BOX = 1;

Q.gravityY = 1500;

Q.Sprite.extend("Player",{

  init: function(p) {

    this._super(p,{
      sheet: "player",
      sprite: "player",
      collisionMask: SPRITE_BOX,
      x: 40,
      y: 600,
      standingPoints: [ [ -16, 44], [ -23, 35 ], [-23,-48], [23,-48], [23, 35 ], [ 16, 44 ]],
      duckingPoints : [ [ -16, 44], [ -23, 35 ], [-23,-10], [23,-10], [23, 35 ], [ 16, 44 ]],
      speed: 400,
      jump: -400
    });

    this.p.points = this.p.standingPoints;

    this.add("2d, animation");
  },

  step: function(dt) {
    this.p.vx += (this.p.speed - this.p.vx)/4;

    if(this.p.y > 600) {
      this.p.y = 600;
      this.p.vy = 0;
      this.p.landed = 1
    }
    else {
      this.p.landed = 0
    }

    if(Q.inputs['fire'] && this.p.y >= 200) {
      if(this.p.y > 210){
        this.p.vy = this.p.jump;
      }
      else{
        this.p.vy = this.p.jump + 390;
      }
    }

    this.p.points = this.p.standingPoints;
    if(this.p.landed) {
      if(Q.inputs['down']) {
        this.play("duck_right");
        this.p.points = this.p.duckingPoints;
      } else {
        this.play("walk_right");
      }
    } else {
      this.play("jump_right");
    }

    this.stage.viewport.centerOn(this.p.x + 400, 400 );

  }
});

Q.Sprite.extend("Box",{
  init: function() {

    var levels = [ 480, 450, 500, 550, 600, 650 ];

    var player = Q("Player").first();
    this._super({
      x: player.p.x + Q.width + 50,
      y: levels[Math.floor(Math.random() * 6)],
      frame: Math.random() < 0.5 ? 1 : 0,
      scale: 3,
      type: SPRITE_BOX,
      sheet: "crates",
      vx: -600 + 200 * Math.random(),
      vy: 0,
      ay: 0,
      theta: 50
    });


    this.on("hit");
  },

  step: function(dt) {
    this.p.x += this.p.vx * dt;


    this.p.vy += this.p.ay * dt;
    this.p.y += this.p.vy * dt;
    if(this.p.y != 565) {
      this.p.angle += this.p.theta * dt;
    }

    if(this.p.y > 800) { this.destroy(); }

  },

  hit: function() {
    this.p.type = 0;
    this.p.collisionMask = Q.SPRITE_NONE;
    this.p.vx = 200;
    this.p.ay = 400;
    this.p.vy = -300;
    this.p.opacity = 0.5;
  }


});

Q.GameObject.extend("BoxThrower",{
  init: function() {
    this.p = {
      launchDelay: 0.75,
      launchRandom: 1,
      launch: 2
    }
  },

  update: function(dt) {
    this.p.launch -= dt;

    if(this.p.launch < 0) {
      this.stage.insert(new Q.Box());
      this.p.launch = this.p.launchDelay + this.p.launchRandom * Math.random();
    }
  }

});


Q.scene("level1",function(stage) {
  //
  // stage.insert(new Q.Repeater({ asset: "background-wall.png",
  //                               speedX: 0.5 }));
  var music = ["1.mp3", "2.mp3", "3.mp3"]

  Q.audio.play(music[Math.floor((Math.random() * 3))])
  stage.insert(new Q.Repeater({ asset: "background-sky.png",
                                repeatY: false}));
  stage.insert(new Q.Repeater({ asset: "snow.png",
                                speedx: 1.0 }));

  stage.insert(new Q.Repeater({ asset: "background-snow.png",
                                repeatY: false,
                                speedX: 1.0,
                                y: 240 }));

  stage.insert(new Q.BoxThrower());

  stage.insert(new Q.Player());
  stage.add("viewport");

});

Q.load("new_year_player.json, new_year_player.png, background-sky.png, background-snow.png, new_year_crates.png, new_year_crates.json, snow.png, 1.mp3, 2.mp3, 3.mp3", function() {
    Q.compileSheets("new_year_player.png","new_year_player.json");
    Q.compileSheets("new_year_crates.png","new_year_crates.json");
    Q.animations("player", {
      walk_right:  { frames: [0], rate: 1/10, flip: false },
      jump_right:  { frames: [0], rate: 1/10, flip: false },
      // stand_right: { frames:[14], rate: 1/10, flip: false },
      // duck_right:  { frames: [15], rate: 1/10, flip: false },
    });
    Q.stageScene("level1");

});


});
