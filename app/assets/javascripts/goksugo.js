//= require jquery
//= require jquery_ujs
//= require quintus.js
//= require quintus_2d.js
//= require quintus_anim.js
//= require quintus_audio.js
//= require quintus_input.js
//= require quintus_scenes.js
//= require quintus_sprites.js
//= require quintus_tmx.js
//= require quintus_touch.js
//= require quintus_ui.js

window.addEventListener("load",function() {
var Q = window.Q = Quintus()
        .include("Sprites, Scenes, Input, 2D, Anim, Touch, UI, Audio")
        .setup({ maximize: true })
        .controls().touch().enableSound();
var SPRITE_BOX = 1;

Q.gravityY = 1300;

var scores = 0;
var lives = 5;
var name = "";

Q.Sprite.extend("Player",{

  init: function(p) {

    this._super(p,{
      sheet: "player",
      sprite: "player",
      collisionMask: SPRITE_BOX,
      x: 40,
      y: 555,
      standingPoints: [ [ -16, 44], [ -23, 35 ], [-23,-48], [23,-48], [23, 35 ], [ 16, 44 ]],
      duckingPoints : [ [ -16, 44], [ -23, 35 ], [-23,-10], [23,-10], [23, 35 ], [ 16, 44 ]],
      speed: 600,
      jump: -700
    });

    this.p.points = this.p.standingPoints;

    this.add("2d, animation");
  },

  step: function(dt) {
    scores = scores + 0.04
    if(scores >= 100 & scores <= 100.04){
      lives = lives + 3;
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px; margin-left: -1px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
    }
    if(scores >= 200 & scores <= 200.04){
      lives = lives + 3;
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px; margin-left: -1px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
    }
    if(scores >= 300 & scores <= 300.04){
      lives = lives + 3;
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px; margin-left: -1px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
    }
    if(scores >= 400 & scores <= 400.04){
      lives = lives + 3;
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px; margin-left: -1px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
    }if(scores >= 500 & scores <= 500.04){
      lives = lives + 3;
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px; margin-left: -1px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
    }if(scores >= 600 & scores <= 600.04){
      lives = lives + 3;
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px; margin-left: -1px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
    }if(scores >= 700 & scores <= 700.04){
      lives = lives + 3;
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px; margin-left: -1px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
      $('#lives').append('<img src="https://img-fotki.yandex.ru/get/5307/50730026.55/0_10e07f_861eaff2_S" width="20px" style="margin-right: 4px;">')
    }
    if((scores >= 50 && scores <= 55) || (scores >= 100 && scores <= 105) ||
       (scores >= 150 && scores <= 155) || (scores >= 200 && scores <= 205) ||
       (scores >= 250 && scores <= 255) || (scores >= 300 && scores <= 305) ||
       (scores >= 350 && scores <= 355) ||(scores >= 400 && scores <= 405) ||
       (scores >= 450 & scores <= 455) || (scores >= 500 && scores <= 555) ||
       (scores >= 600 & scores <= 605) || (scores >= 650 && scores <= 655) ||
       (scores >= 700 & scores <= 705) || (scores >= 1000 && scores <= 1005)){
      $('.rooney').show('slow');
    }
    else{
      $('.rooney').hide('slow');
    }
    if((scores >= 51 & scores <= 51.04) || (scores >= 101 & scores <= 101.04) ||
      (scores >= 151 & scores <= 151.04) || (scores >= 201 & scores <= 201.04) ||
      (scores >= 251 & scores <= 251.04) ||
      (scores >= 301 & scores <= 301.04) || (scores >= 351 & scores <= 351.04) ||
      (scores >= 401 & scores <= 401.04) || (scores >= 451 & scores <= 451.04) ||
      (scores >= 501 & scores <= 501.04) ||
      (scores >= 601 & scores <= 601.04) || (scores >= 651 & scores <= 651.04) ||
      (scores >= 701 & scores <= 701.04) || (scores >= 1001 & scores <= 1001.04)){
      Q.audio.play('rooney.mp3')
    }
    $('#scores').html("Очки: " + Math.floor(scores))
    this.p.speed = this.p.speed + 0.2;
    this.p.vx += (this.p.speed - this.p.vx)/4;

    if(this.p.y > 555) {
      this.p.y = 555;
      this.p.landed = 1;
      this.p.vy = 0;
    } else {
      this.p.landed = 0;
    }

    if(Q.inputs['up'] && this.p.landed > 0) {
      this.p.vy = this.p.jump;
    }

    this.p.points = this.p.standingPoints;
    if(this.p.landed) {
      if(Q.inputs['down']) {
        this.play("duck_right");
        this.p.points = this.p.duckingPoints;
      } else {
        this.play("walk_right");
      }
    } else {
      this.play("jump_right");
    }

    this.stage.viewport.centerOn(this.p.x + 300, 400 );

  }
});


Q.Sprite.extend("Box",{

  init: function() {
    var levels = [ 440, 550, 600 ];
    var player = Q("Player").first();
    this._super({
      x: Q.width + player.p.x + 50,
      y: levels[Math.floor(Math.random() * 3)],
      frame: Math.floor(Math.random() * (6 - 0 + 1)) + 0,
      scale: 2.5,
      type: SPRITE_BOX,
      sheet: "crates",
      vx: -200 + 200 * Math.random(),
      vy: 0,
      ay: 0,
      theta: 00
    });
    this.on("hit");
  },

  step: function(dt) {

    this.p.x += this.p.vx * dt;

    this.p.vy += this.p.ay * dt;
    this.p.y += this.p.vy * dt;
    if(this.p.y != 565) {
      this.p.angle += this.p.theta * dt;
    }

    if(this.p.y > 800) { this.destroy(); }

  },


  hit: function() {
    var element = document.getElementById("lives");
    if(this.p.opacity != 0.5){
      lives = lives - 1;
      $('#lives').find('img').last().remove();
      Q.audio.play('damn.mp3');
    }
    console.log(this.p)
    this.p.type = 0;
    this.p.collisionMask = Q.SPRITE_NONE;
    this.p.vx = 200;
    this.p.ay = 400;
    this.p.vy = -300;
    this.p.opacity = 0.5;

    if (lives < 1){
      Q.stageScene("endGame");
      if($('#scores').html().indexOf('Начать') <= 0){
        $('#scores').append('<br><a class="restart_game" href="/">Начать заново</a>')
        $('.vk').html(VK.Share.button({title: 'Я набрал(а) ' + Math.floor(scores) + ' очков! Сможешь больше?', image: 'http://free-bet.ru/goksu.png', noparse: true}, {type: 'button', text: 'Поделиться'}));
      }
    }

  }


});

Q.GameObject.extend("BoxThrower",{
  init: function() {
    this.p = {
      launchDelay: 1,
      launchRandom: 1,
      launch: 2
    }
  },

  update: function(dt) {
    this.p.launch -= dt;

    if(this.p.launch < 0) {
      this.stage.insert(new Q.Box());
      this.p.launch = this.p.launchDelay + this.p.launchRandom * Math.random();
    }

  }

});


Q.scene("level1",function(stage) {

  stage.insert(new Q.Repeater({ asset: "background-wall.png", speedX: 0.5 }));
  stage.insert(new Q.Repeater({ asset: "background-floor.png", repeatY: false, speedX: 1.0, y: 300 }));
  stage.insert(new Q.BoxThrower());

  stage.insert(new Q.Player());

  stage.add("viewport");

});

Q.scene("level2",function(stage) {

  stage.insert(new Q.Repeater({ asset: "background-wall.png",
                                speedX: 0.5 }));

  stage.insert(new Q.Repeater({ asset: "background-floor.png",
                                repeatY: false,
                                speedX: 1.0,
                                y: 300 }));

  stage.insert(new Q.BoxThrower());

  stage.insert(new Q.PlayerInCar());
  stage.add("viewport");

});


Q.scene("start",function(stage) {

  $('form').removeClass('hidden');
  $('input.submit').click(function(ev){
    ev.preventDefault();
    name = $('form input').val();
    $('form').addClass('hidden');
    Q.stageScene('level1');
  })

});

Q.scene("endGame",function(stage) {
  Q.audio.play('damn.mp3');
  $('#scores').css('color', "#fff")
  $('#scores').css('text-align', "center")
  $('#scores').css('margin', "0 auto")
  $('#scores').css('width', "100%")
  $('#scores').css('font-size', "40px")
  $('#scores').css('margin-top', "40px")
  $('.refresh').css('display', 'block')
  var element = document.getElementById("game_over");
  element.style.display = "block";


  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200){
    }
  }
  request.open("GET", "/api/scores/?score="+scores+"&name="+name, true);
  request.send(null);
});

Q.load("player.json, player.png, background-wall.png, background-floor.png, crates.png, crates.json, damn.mp3, game_over.png, rooney.png, rooney.mp3, woody.mp3", function() {
    Q.compileSheets("player.png","player.json");
    Q.compileSheets("crates.png","crates.json");
    Q.animations("player", {
      walk_right:  { frames: [0,1,2,3,4,5,6,7,8,9,10], rate: 1/15, flip: false, loop: true },
      jump_right:  { frames: [13], rate: 1/10, flip: false },
      stand_right: { frames:[14], rate: 1/10, flip: false },
      duck_right:  { frames: [15], rate: 1/10, flip: false },
    });
    Q.audio.play('woody.mp3');
    Q.stageScene("level1");

});


});
